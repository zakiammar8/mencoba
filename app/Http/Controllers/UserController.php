<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function user()
    {
        return view('layout.masthead');
    }

    public function producta()
    {
        return view('layout.product');
    }
}
