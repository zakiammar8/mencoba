<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/category', function () {
    return view('pages.categories');
});

Route::get('/','UserController@user');

Route::get('/signin', function() {
    return view('layout.signin');
});

Route::get('/barang', function() {
    return view('layout.product');
});