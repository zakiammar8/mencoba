<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title> Katalog Fashion </title>
        <!-- Favicon-->
        <link href="{{asset('tempUser/bootstrap.bundle.min.js / bootstrap.bundle.js ')}}">
        <link rel="icon" type="image/x-icon" href="{{asset('tempUser/assets/favicon.ico')}}"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Font Awesome icons (free version)-->
        <script type="text/javascript" src="Scripts/bootstrap.dropdown.js"></script>
        <script type="text/javascript" src="Scripts/jquery-2.1.1.min.js"></script>
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="{{asset('tempUser/https://fonts.googleapis.com/css?family=Montserrat:400,700')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('tempUser/https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700')}}" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{asset('tempUser/css/styles.css')}}" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        @include('layouts.navbar')
        <!-- Masthead-->
        @yield('masthead')
        <!-- Services-->
        
        <!-- Portfolio Grid-->
        <!-- Portfolio Grid-->
        @yield('product')
        <!-- About-->
        
        <!-- Team-->
        
        <!-- Clients-->

        <!-- Contact-->
        @yield('signin')

                <!-- * * * * * * * * * * * * * * *-->
                <!-- * * SB Forms Contact Form * *-->
                <!-- * * * * * * * * * * * * * * *-->
                
        <!-- Footer-->
        <!-- Portfolio Modals-->
        @yield('popup')

        <!-- Portfolio item 1 modal popup-->
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{asset('tempUser/js/scripts.js')}}"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 text-lg-start">
                    Copyright &copy; Pluviophile 2022
                </div>
                <div class="col-lg-4 my-3 my-lg-0">
                    <a
                        class="btn btn-dark btn-social mx-2"
                        href="#!"
                        aria-label="Twitter"
                        ><i class="fab fa-twitter"></i
                    ></a>
                    <a
                        class="btn btn-dark btn-social mx-2"
                        href="#!"
                        aria-label="Facebook"
                        ><i class="fab fa-facebook-f"></i
                    ></a>
                    <a
                        class="btn btn-dark btn-social mx-2"
                        href="#!"
                        aria-label="LinkedIn"
                        ><i class="fab fa-linkedin-in"></i
                    ></a>
                </div>
            </div>
        </div>
    </footer>
</html>
