@extends('layout.user')
@section('masthead')
<header class="masthead">
    <div class="container">
        <div class="masthead-subheading" >Welcome To Our Studio!</div>
        <div class="masthead-heading text-uppercase" >It's Nice To Meet You</div>
        <a class="btn btn-secondary btn-xl text-uppercase" href="/barang">explore</a>
    </div>
</header>
@endsection